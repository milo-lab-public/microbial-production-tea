# Microbial production TEA

This repository contains the data and code for the analysis found in "Electro-microbial production techno-economic viability and environmental implications" by Samuel J. Lovat, Roee Ben-Nissan, Eliya Milshtein, Asaf Tzachor, Avi Flamholz, Dorian Leger, Elad Noor, Ron Milo

microbial_production_tea_calculations_figure.ipynb | A Python script that harmonises environmental impact values and product prices for current food and fuel items derived from third party sources given across different functional units and system boundaries for consistency. These values are then compared with the environmental impacts of microbial products. The script includes the code for generating Fig. 2 in the main text of this manuscript and Figs. S3 and S4.

other_env_impacts_and_nutritive_factors.csv | Contains environmental impact and chemical property values not included elsewhere in the other data files used in this study from third party sources. 

### Additional data files required to run our code

A description of the files required to run the code can be found in the Jupyter Notebook with instructions on where to obtain the data. 

### Project dependencies
In order to run the code in this repository, first install the dependencies of the code. These packages can be installed by running: pip install -r requirements.txt

